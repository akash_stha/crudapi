module.exports = function mapData(obj1, obj2) {

    if (obj2.name)
        obj1.name = obj2.name
    if (obj2.origin)
        obj1.origin = obj2.origin
    if (obj2.category)
        obj1.category = obj2.category
    if (obj2.quantity)
        obj1.quantity = obj2.quantity
    if (obj2.price)
        obj1.price = obj2.price
    if (obj2.manuDate)
        obj1.manuDate = new Date(obj2.manuDate)
    if (obj2.expiryDate)
        obj1.expiryDate = new Date(obj2.expiryDate)
    if (obj2.image)
        obj1.image = obj2.image
    if (obj2.tags)
        obj1.tags = obj2.tags.split(',')


    return obj1
}
