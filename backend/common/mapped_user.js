function mapUser(obj1, obj2) {
    if (obj2.name)
        obj1.name = obj2.name
    if (obj2.email)
        obj1.email = obj2.email
    if (obj2.username)
        obj1.username = obj2.username
    if (obj2.password)
        obj1.password = obj2.password
    if (obj2.hobbies)
        obj1.hobbies = obj2.hobbies.split(',')
    if (obj2.phone)
        obj1.phone = obj2.phone

    return obj1
}

module.exports = mapUser