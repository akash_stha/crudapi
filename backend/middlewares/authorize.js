module.exports = function(req, res, next){
    console.log('req.loggedInUser is', req.loggedInUser.role)
    if(req.loggedInUser.role == 1){
        return next()
    }else{
        return next({
            message: 'you don\'t have an access',
            status: 403
        })
    }
}