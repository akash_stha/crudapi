var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var productSchema = new Schema({
    name:{
        type: String,
        required: 'please enter the product name',
        uppercase: true
    },
    category:{
        type:String,
        required: 'please enter the category of product'
    },
    origin:{
        type: String,
        uppercase: true
    },
    quantity:{
        type: Number
    },
    price:{
        type: Number
    },
    manuDate:{
        type: Date
    },
    expiryDate:{
        type: Date
    },
    image:{
        type:String
    },
    tags:{
        type:[String]
    },
    user:{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
},{
    timestamps: true
})

var productModel = mongoose.model('product', productSchema)

module.exports = productModel