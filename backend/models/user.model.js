var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name:{
        type:String,
        uppercase: true,
        required: 'please enter the name'
    },
    email:{
        type: String,
        unique: true,
        trim: true,
        sparse: true
    },
    username:{
        type: String,
        unique: true,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    phone:{
        type: Number
    },
    hobbies:[String],
    role:{
        type: String,
        enum: ['1','2'],
        default: 2
    }
},{
    timestamps: true
})

var userModel = mongoose.model('user', userSchema)

module.exports = userModel