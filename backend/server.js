var express = require('express')
var app = express()
var morgan = require('morgan')
var path = require('path')
var config = require('./config')
require('./db')


var authRoute = require('./controllers/auth')
var userRoute = require('./controllers/user')
var productRoute = require('./controllers/product')

var authenticate = require('./middlewares/authentication')

app.use(morgan('dev'))

app.use('/img', express.static(path.join(__dirname, 'files')))

app.use(express.urlencoded({
    extended: true
}))
app.use(express.json())

app.use('/auth', authRoute)
app.use('/user', authenticate, userRoute)
app.use('/product', authenticate, productRoute)

app.use(function(req, res, next){
    console.log('i am a 404 handler middleware')
    next({
        message: 'PAGE NOT FOUND',
        status: 404
    })
})

app.use(function(err, req, res, next){
    console.log('i am an error handling middleware',err)
    res.status(err.status || 400)
    res.json({
            message: err.message || err,
            status: err.status || 400
    })
})

app.listen(config.port, function(err, done){
    if(err){
        console.log('server connection failed')
    }else{
        console.log('server connected at port ',config.port)
    }
})
