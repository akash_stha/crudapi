var mongoose = require('mongoose')
var dbUrl = 'mongodb://localhost:27017/esewa'

mongoose.connect(dbUrl, { useNewUrlParser: true }, function(err, done){
    if(err){
        console.log('db connection failed',err)
    }else{
        console.log('db connection success')
    }
})