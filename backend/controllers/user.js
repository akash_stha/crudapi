var express = require('express')
var router = express.Router();
var userModel = require('./../models/user.model')

router.get('/', function (req, res, next) {
    userModel.find({})
        .sort({
            _id: -1
        })
        .exec(function (err, user) {
            if (err) {
                return next(err)
            } else {
                res.status(200).json(user)
            }
        })
})

router.get('/:id', function (req, res, next) {
    userModel.findById(req.params.id, function(err, user){
        if(err){
            return next(err)
        }
        if(user){
            res.status(200).json(user)
        }else{
            next({
                message: 'user not found',
                status: 404
            })
        }
    })
})

router.put('/:id', function(req, res, next){
    userModel.findByIdAndUpdate({_id:req.params.id}, req.body, {new: true}, function(err, done){
        if(err){
            return next(err)
        }else{ 
            res.status(200).json(done)
        }
    })
})

router.delete('/:id', function(req, res, next){
    userModel.findByIdAndDelete({_id:req.params.id}, function(err, done){
        if(err){
            return next(err)
        }else{
            res.status(200).json('user deleted')
        }
    })
})


module.exports = router;