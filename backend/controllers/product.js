var express = require('express')
var router = express.Router();
var productModel = require('./../models/product.model')
var mapProduct = require('./../common/mapped_product')
var multer = require('multer')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})

var upload = multer({
    storage: storage
})

router.post('/', upload.single('image'), function (req, res, next) {
    var newProduct = new productModel(req.body)
    var mappedProduct = mapProduct(newProduct, req.body)

    newProduct.user = req.loggedInUser._id
    newProduct.image = req.file.filename

    mappedProduct.save(function (err, product) {
        if (!err) {
            res.status(200).json(product)
        } else {
            return next(err)
        }
    })
})

router.get('/', function (req, res, next) {
    productModel.find({})
        .sort({
            _id: -1
        })
        .exec(function (err, product) {
            if (err) {
                return next(err)
            }else{
                res.status(200).json(product)
            }
        })

})

router.get('/:id', function(req, res, next){
    productModel.findById(req.params.id, function(err, product){
        if(err){
            return next(err)
        }
        if(product){
            res.status(200).json(product)
        }
        else {
            next({
                message: 'product not found',
                status: 404
            })
        }
    })
})

router.put('/:id', upload.single('image') , function(req, res, next){
    productModel.findByIdAndUpdate({_id:req.params.id}, req.body, {new:true}, function(err, product){
        if(err){
            return next(err)
        }else{
            res.status(200).json(product)
        }
    })
})

router.delete('/:id', function(req, res, next){
    productModel.remove({_id:req.params.id}, function(err, product){
        if(err){
            return next(err)
        }else{
            res.status(200).json('product deleted');
        }
    })
})

module.exports = router