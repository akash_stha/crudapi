var express = require('express')
var router = express.Router()
var userModel = require('./../models/user.model')
var mapUser = require('./../common/mapped_user')
var config = require('./../config')
var jwt = require('jsonwebtoken')
var bcrypt = require('bcryptjs')

function createToken(data){
    var token = jwt.sign({
        username: data.username,
        id:data._id,
        role: data.role
    },config.jwtSecret,{
        expiresIn: '10m'
    })
    return token;
}

router.post('/', function(req, res, next){
    userModel.findOne({
        username: req.body.username
    },function(err, user){
        if(err){
            return next(err)
        }
        if(user){
            bcrypt.compare(req.body.password, user.password, function(err, matched){
                if(matched){
                    var token = createToken(user)
                    res.status(200).json({
                        user: user,
                        token: token
                    })
                }else{
                    next({
                        message: 'Password doesn\'t match',
                        status: 403
                    })
                }
            })
        }else{
            next({
                message: 'User not found',
                status: 403
            })
        }
    })
})

router.post('/register', function(req, res, next){
    var newUser = new userModel(req.body)
    var mappedUser = mapUser(newUser, req.body)

    bcrypt.hash(req.body.password, 10, function(err, hash){
        if(err){
            return next(err)
        }else{
            mappedUser.password = hash
            mappedUser.save(function(err, done){
                if(err){
                    return next(err)
                }else{
                    res.status(200).json(done)
                }
            })
        }
    })

})

module.exports = router